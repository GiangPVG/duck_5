﻿using ShopBug.Model;
using ShopBug.Model.Dao;
using ShopBug.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopBug.Web.Areas.Admin.Controllers
{
    public class MenuController : Controller
    {
      
        // GET: Admin/Menu
        public ActionResult Index()
        {
            var dao = new MenuDao().ListAll();
            return View(dao);
        }

        [HttpPost]
        public JsonResult ChangeStatus(int id)
        {
            var result = new MenuDao().ChangeStatus(id);
            return Json(new
            {
                status = result
            });
        }

        [HttpDelete]
        public ActionResult Delete(int id)
        {

            new MenuDao().Delete(id);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Create() {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Menu menu) {
            var dao = new MenuDao();
            int id = dao.Insert(menu);
            if (id > 0) {
                return RedirectToAction("Index", "Menu");
            }
            return View("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var menu = new MenuDao().ViewDetail(id);
            return View(menu);
        }

        [HttpPost]
        public ActionResult Edit(Menu menu) {
            var dao = new MenuDao();
            var result = dao.Update(menu);
            if (result)
            {
                return RedirectToAction("Index", "Menu");
            }
            else
            {
                ModelState.AddModelError("", "Update Menu not successfully");
            }

            return View("Index");
        }
    }
}