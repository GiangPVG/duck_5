﻿using ShopBug.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopBug.Model.Dao
{
    public class OrderDao
    {
        ShopBugDbContext db = null;
        public OrderDao()
        {
            db = new ShopBugDbContext();
        }
        public long InsertOrder(Order order)
        {
            try
            {
               
                db.Orders.Add(order);
                db.SaveChanges();
                return order.ID;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public Order ViewDetail(long id)
        {
            return db.Orders.Find(id);
        }

        public bool Delete(int id)
        {
            try
            {
                var orderDetial = db.OrderDetails.Where(x => x.OrderID.Equals(id)).ToList();
                foreach (var item in orderDetial) {
                    db.OrderDetails.Remove(item);
                    db.SaveChanges();
                }
                var order = db.Orders.Find(id);
                db.Orders.Remove(order);
                db.SaveChanges();
                return true;

            }
            catch (Exception e)
            {
                return false;
            }


        }

        public IEnumerable<Order> Details()
        {
            return db.Orders.OrderByDescending(x => x.ID).ToList();
        }

        public bool ChangeStatus(long id)
        {
            var order = db.Orders.Find(id);
            order.Status = !order.Status;

            if (order.Status == true)
            {
                db.SaveChanges();
            }
            else {
                order.Status = !order.Status;
            }
            return !order.Status;
        }
    }
}

