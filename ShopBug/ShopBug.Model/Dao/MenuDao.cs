﻿using ShopBug.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopBug.Model.Dao

{
    public class MenuDao
    {
        ShopBugDbContext db = null;

        public MenuDao()
        {
            db = new ShopBugDbContext();
        }

        public List<Menu> ListByGroupID(int groupid)
        {
            return db.Menus.Where(x => x.GroupID == groupid).ToList();
        }

        public List<Menu> ListAll() {
            return db.Menus.Where(x => x.DisplayOrder > 0).ToList();
        }

        public bool ChangeStatus(int id)
        {
            var menu = db.Menus.Find(id);
            menu.Status = !menu.Status;
            db.SaveChanges();

            return !menu.Status;
        }

        public bool Delete(int id)
        {
            try
            {              
                var menu = db.Menus.Find(id);
                db.Menus.Remove(menu);
                db.SaveChanges();
                return true;

            }
            catch (Exception e)
            {
                return false;
            }
        }

        public int Insert(Menu entity)
        {
            db.Menus.Add(entity);
            db.SaveChanges();
            return entity.ID;
        }

        public Menu ViewDetail(int id)
        {
            return db.Menus.Find(id);
        }

        public bool Update(Menu entity) {
            try
            {
                var menu = db.Menus.Find(entity.ID);
                menu.Name = entity.Name;
                menu.URL = entity.URL;
                menu.DisplayOrder = entity.DisplayOrder;
                menu.Target = entity.Target;
                menu.Status = entity.Status;
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }


    }
}
