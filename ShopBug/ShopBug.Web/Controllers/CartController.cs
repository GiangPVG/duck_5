﻿using ShopBug.Common.SendEmail;
using ShopBug.Model;
using ShopBug.Model.Dao;
using ShopBug.Model.Models;
using ShopBug.Web.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ShopBug.Web.Controllers
{
    public class CartController : Controller
    {
        private const string CartSession = "CartSession";
        private const string CartSession1 = "CartSession1";
        // GET: Cart
        public ActionResult Index()
        {
            var cart = Session[CartSession];
            var list = new List<CartItem>();
            if (cart != null)
            {
                list = (List<CartItem>)cart;
            }

            return View(list);
        }
        public ActionResult AddItem(long productId, int quantity)
        {
            var product = new ProductDao().ViewDetail(productId);
            var cart = Session[CartSession];
            if (cart != null)
            {

                var list = (List<CartItem>)cart;
                if (list.Exists(x => x.Product.ID == productId))
                {
                    foreach (var item in list)
                    {
                        if (item.Product.ID == productId)
                        {
                            item.Quantity += quantity;
                        }
                    }
                }
                else
                {
                    //add Cart Item
                    var item = new CartItem();
                    item.Product = product;
                    item.Quantity = quantity;
                    list.Add(item);
                }
                Session[CartSession] = list;
            }
            else
            {
                //add CartItem
                var item = new CartItem();
                item.Product = product;
                item.Quantity = quantity;
                var list = new List<CartItem>();
                list.Add(item);
                //Gán vào session
                Session[CartSession] = list;
            }
            return RedirectToAction("Index");
        }
        public JsonResult DeleteAll()
        {
            Session[CartSession] = null;
            return Json(new
            {
                status = true
            });
        }

        public JsonResult Delete(long id)
        {
            var sessionCart = (List<CartItem>)Session[CartSession];
            sessionCart.RemoveAll(x => x.Product.ID == id);
            Session[CartSession] = sessionCart;
            return Json(new
            {
                status = true
            });
        }


        public JsonResult Update(string cartModel)
        {
            var jsonCart = new JavaScriptSerializer().Deserialize<List<CartItem>>(cartModel);
            var sessionCart = (List<CartItem>)Session[CartSession];

            foreach (var item in sessionCart)
            {
                var jsonItem = jsonCart.SingleOrDefault(x => x.Product.ID == item.Product.ID);
                if (jsonItem != null)
                {
                    item.Quantity = jsonItem.Quantity;
                }
            }
            Session[CartSession] = sessionCart;
            return Json(new
            {
                status = true
            });
        }



        [HttpGet]
        public ActionResult Payment()
        {
            var cart = Session[CartSession];
            var list = new List<CartItem>();
            if (cart != null)
            {
                list = (List<CartItem>)cart;
            }
            return View(list);
        }
        [HttpPost]
        public ActionResult Payment(string name, string mobile, string address, string email)
        {
            var custommer = new Customer();
            custommer.CreatedDate = DateTime.Now;
            custommer.Name = name;
            custommer.Phone = mobile;
            custommer.Address = address;
            custommer.Email = email;
            try
            {
                var Id = new CustomerDao().InsertCustomer(custommer);
                var order = new Order();
                order.ModifiedDate = DateTime.Now;
                order.CustomerID = custommer.ID;
                order.CustomerName = custommer.Name;
                order.CustomerMobile = custommer.Phone;
                order.CustomerAddress = custommer.Address;
                order.CustomerEmail = custommer.Email;
                try
                {
                    decimal total = 0;

                    var id = new OrderDao().InsertOrder(order);
                    var cartsession = new CartItem();
                    cartsession.OrderID = id;
                    var list = new List<CartItem>();
                    list.Add(cartsession);
                   
                    var cart = (List<CartItem>)Session[CartSession];
                    var detailDao = new OrderDetailDao();
                    foreach (var item in cart)
                    {
                         total = 0;
                        var orderDetail = new OrderDetail();
                        orderDetail.ProductID = item.Product.ID;
                        orderDetail.OrderID = id;
                        orderDetail.Price = item.Product.Price;
                        orderDetail.Quantity = item.Quantity;
                        total += (item.Product.Price.GetValueOrDefault(0) * item.Quantity);
                        orderDetail.Total = total;
                        detailDao.InsertOrderDetail(orderDetail);

                    }
                    #region
                    //if (ModelState.IsValid)
                    //{
                    //    var emailname = name;
                    //    var phonename = mobile;
                    //    var sendname = new MailAddress(email, "Email");
                    //    var addressname = address;
                    //    var senderemail = new MailAddress("haunguyen200498@gmail.com", name);
                    //    var password = "Hhhh1234";
                    //    var smtp = new SmtpClient
                    //    {
                    //        Host = "smtp.gmail.com",
                    //        Port = 587,
                    //        EnableSsl = true,
                    //        DeliveryMethod = SmtpDeliveryMethod.Network,
                    //        UseDefaultCredentials = false,
                    //        Credentials = new NetworkCredential(senderemail.Address, password)
                    //    };
                    //    using (var mess = new MailMessage(sendname, senderemail)
                    //    {
                    //        Subject = name + ": Đơn hàng mới ",
                    //        Body = "Name : " + name + " " + "Email :" + email + " " + "Phone :" + mobile + " " + "Address :" + address
                    //    }
                    //    )
                    //    {
                    //        smtp.Send(mess);
                    //    }

                    //    using (var mess2 = new MailMessage(senderemail, sendname)
                    //    {
                    //        Subject = name + ": Thông tin đơn hàng của bạn",
                    //        Body = "Name : " + name + " " + "Email :" + email + " " + "Phone :" + mobile + " " + "Address :" + address
                    //    }
                    //    )
                    //    {
                    //        smtp.Send(mess2);
                    //    }
                    #endregion
                    Session[CartSession1] = list;


                    //}
                    string content = System.IO.File.ReadAllText(Server.MapPath("~/Assets/client/template/donhang.html"));

                    content = content.Replace("{{CustomerName}}", name);
                    content = content.Replace("{{Phone}}", mobile);
                    content = content.Replace("{{Email}}", email);
                    content = content.Replace("{{Address}}", address);
                    content = content.Replace("{{Total}}", total.ToString("N0"));
                    var toEmail = ConfigurationManager.AppSettings["ToEmailAddress"].ToString();
                    var thongtinsp = ConfigurationManager.AppSettings["xacnhan"].ToString();
                    content = content.Replace("{{Details}}", thongtinsp);

                    new MailHepper().SendMail(email, "Đơn hàng mới từ Shop", content);
                    new MailHepper().SendMail(toEmail, "Đơn hàng mới từ Shop", content);
                }
                catch (Exception)
                {
                    return Redirect("/loi-thanh-toan");
                    throw;
                }

            }
            catch (Exception)
            {
                return Redirect("/loi-thanh-toan");

                throw;
            }

            return Redirect("/xac-nhan");

        }
        public ActionResult Success()
        {
            return View();
        }

        public ActionResult Details()
        {
            ShopBugDbContext db = new ShopBugDbContext();

            //    long id = db.OrderDetails.or
            //    var product = db.Products.Find(id);
            //    return View(product);
            var cart = (List<CartItem>)Session[CartSession1];
            foreach (var item in cart) {
                long id = item.OrderID;
                var order = db.Orders.Find(id);
                return View(order);
            }
            return View();
        }
    }
}