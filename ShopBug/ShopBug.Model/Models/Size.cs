﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopBug.Model.Models
{
    [Table("Size")]
    public partial class Size
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }
    }
}
