﻿function ShowImagePreview(fileUpload, previewImage) {
    if (fileUpload.files && fileUpload.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(previewImage).attr('src', e.target.result);
        };
        reader.readAsDataURL(fileUpload.files[0]);
    }
}