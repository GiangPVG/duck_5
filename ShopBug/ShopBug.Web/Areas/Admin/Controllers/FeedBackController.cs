﻿using PagedList;
using ShopBug.Model;
using ShopBug.Model.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopBug.Web.Areas.Admin.Controllers
{
    public class FeedBackController : Controller
    {
        ShopBugDbContext db = new ShopBugDbContext();
        // GET: Admin/FeedBack
        public ActionResult Index(int? page)
        {
            var pageNumber = page ?? 1;
            var pageSize = 10;
            var feedback = db.FeedBacks.OrderByDescending(x => x.ID).ToPagedList(pageNumber, pageSize);

            return View(feedback);
        }
        [HttpDelete]
        public ActionResult Delete(int id) {
            new ContactDao().Delete(id);
            return RedirectToAction("Index");
        }
    }
}