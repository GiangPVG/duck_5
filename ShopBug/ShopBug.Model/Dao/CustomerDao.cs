﻿using PagedList;
using ShopBug.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopBug.Model.Dao
{

    public class CustomerDao
    {
        ShopBugDbContext db = null;
        public CustomerDao()
        {
            db = new ShopBugDbContext();
        }
        public long InsertCustomer(Customer customer)
        {
            db.Customers.Add(customer);
            db.SaveChanges();
            return customer.ID;
        }
        public IEnumerable<Customer> Details()
        {
            return db.Customers.OrderByDescending(x => x.ID).ToList();
        }

        public Customer ViewDetail(long id)
        {
            return db.Customers.Find(id);
        }

        public bool ChangeStatus(long id)
        {
            var customer = db.Customers.Find(id);
            customer.Status = !customer.Status;
            db.SaveChanges();

            return !customer.Status;
        }

        public bool Delete(int id)
        {

            try
            {
                var customer = db.Customers.Find(id);
                db.Customers.Remove(customer);
                db.SaveChanges();
                return true;

            }
            catch (Exception e)
            {
                return false;
            }


        }

        public IEnumerable<Customer> ListAllPaging(string searchString, int page, int pageSize)
        {
            IQueryable<Customer> model = db.Customers;
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.Name.Contains(searchString) || x.Name.Contains(searchString));
            }

            return model.OrderByDescending(x => x.CreatedDate).ToPagedList(page, pageSize);
        }

        public bool Update(Customer entity) {
            try
            {
                var customer = db.Customers.Find(entity.ID);
                customer.Name = entity.Name;
                customer.Address = entity.Address;
                customer.Email = entity.Email;
                customer.Phone = entity.Phone;
                customer.CreatedDate = entity.CreatedDate;
                customer.ModifiedDate = DateTime.Now;
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
    }
}
