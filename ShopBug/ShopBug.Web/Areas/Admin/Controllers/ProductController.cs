﻿using ShopBug.Model.Dao;
using ShopBug.Model.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopBug.Web.Areas.Admin.Controllers
{
    public class ProductController : Controller
    {
        // GET: Admin/Product
        public ActionResult Index(string searchString, int page = 1, int pageSize = 5)
        {
            var dao = new ProductDao();
            var model = dao.ListAllPaging(searchString, page, pageSize);

            ViewBag.SearchString = searchString;
            return View(model);
        }

        [HttpGet]
        public ActionResult Create() {
            Product product = new Product();
            return View(product);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(Product product, HttpPostedFileBase fileUpload , int sale) {

            var dao = new ProductDao();
            var filename = Path.GetFileName(fileUpload.FileName);
            var path = Path.Combine(Server.MapPath("/Assets/client/images/kamitos/"), filename);

            if (System.IO.File.Exists(path)) {
                ViewBag.ThongBao = "Hình ảnh đã tồn tại";
            } else if (fileUpload == null) {
                ViewBag.ThongBao = "Chọn hình ảnh";
                return View();
            }
            else
            {
                fileUpload.SaveAs(path);
            }

            product.Image = "/Assets/client/images/kamitos/" + fileUpload.FileName;
            product.PromotionPrice = product.Price;
            product.Price = (product.Price * (100-sale)) / 100;
            long id = dao.Insert(product);
            if (id > 0) {
                return RedirectToAction("Index", "Product");
            }
            return View("Index");
        }

        [HttpDelete]
        public ActionResult Delete(int id)
        {

            new ProductDao().Delete(id);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult ChangeStatus(long id)
        {
            var result = new ProductDao().ChangeStatus(id);
            return Json(new
            {
                status = result
            });
        }

        [HttpGet]
        public ActionResult Edit(long id)
        {
            var product = new ProductDao().ViewDetail(id);
            return View(product);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(Product product , HttpPostedFileBase fileUpload) {
            var dao = new ProductDao();
            var filename = Path.GetFileName(fileUpload.FileName);
            var path = Path.Combine(Server.MapPath("/Assets/client/images/kamitos/"), filename);
            if (System.IO.File.Exists(path))
            {
                ViewBag.ThongBao = "Hình ảnh đã tồn tại";
            }
            else if (fileUpload == null)
            {
                ViewBag.ThongBao = "Chọn hình ảnh";
                return View();
            }
            else
            {
                fileUpload.SaveAs(path);
            }
            product.Image = "/Assets/client/images/kamitos/" + fileUpload.FileName;

            var result = dao.Update(product);
            if (result)
            {
                return RedirectToAction("Index", "Product");
            }
            else
            {
                ModelState.AddModelError("", "Update Product not successfully");
            }

            return View("Index");
        }

    }
}