﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ShopBug.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
              name: "Contact",
              url: "lien-he",
              defaults: new { controller = "Contact", action = "Index", id = UrlParameter.Optional },
              namespaces: new[] { "ShopBug.Web.Controllers" }
          );

            routes.MapRoute(
            name: "Product Detail",
            url: "chi-tiet/{metatitle}-{id}",
            defaults: new { controller = "Product", action = "Detail", id = UrlParameter.Optional },
            namespaces: new[] { "ShopBug.Web.Controllers" }
           );

            routes.MapRoute(
            name: "Add Cart",
            url: "them-gio-hang",
            defaults: new { controller = "Cart", action = "AddItem", id = UrlParameter.Optional },
            namespaces: new[] { "ShopBug.Web.Controllers" }
           );

            routes.MapRoute(
            name: "Payment ",
            url: "thanh-toan",
            defaults: new { controller = "Cart", action = "Payment", id = UrlParameter.Optional },
            namespaces: new[] { "ShopBug.Web.Controllers" }
           );
            routes.MapRoute(
            name: "Success ",
            url: "hoan-thanh",
            defaults: new { controller = "Cart", action = "Success", id = UrlParameter.Optional },
            namespaces: new[] { "ShopBug.Web.Controllers" }
           );

            routes.MapRoute(
            name: "About ",
            url: "gioi-thieu",
            defaults: new { controller = "About", action = "Index", id = UrlParameter.Optional },
            namespaces: new[] { "ShopBug.Web.Controllers" }
           );

            routes.MapRoute(
          name: "Xacnhan",
          url: "xac-nhan",
          defaults: new { controller = "Cart", action = "Details", id = UrlParameter.Optional },
          namespaces: new[] { "ShopBug.Web.Controllers" }
         );

            routes.MapRoute(
            name: "Payment",
            url: "thanh-toan",
            defaults: new { controller = "Cart", action = "Payment", id = UrlParameter.Optional },
            namespaces: new[] { "ShopBug.Web.Controllers" }
           );

            routes.MapRoute(
                name: "Product",
                url: "san-pham",
                defaults: new { controller = "Product", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "ShopBug.Web.Controllers" }
            );
            routes.MapRoute(
               name: "Test",
               url: "san-pham-nike",
               defaults: new { controller = "Product", action = "Test", id = UrlParameter.Optional },
               namespaces: new[] { "ShopBug.Web.Controllers" }
           );

            routes.MapRoute(
               name: "Search",
               url: "tim-kiem",
               defaults: new { controller = "Product", action = "Search", id = UrlParameter.Optional },
               namespaces: new[] { "ShopBug.Web.Controllers" }
           );
            routes.MapRoute(
                name: "ProductSale",
                url: "san-pham-khuyen-mai",
                defaults: new { controller = "ProductSale", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "ShopBug.Web.Controllers" }
            );

            routes.MapRoute(
               name: "New",
               url: "tin-tuc",
               defaults: new { controller = "New", action = "Index", id = UrlParameter.Optional },
               namespaces: new[] { "ShopBug.Web.Controllers" }
           );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "ShopBug.Web.Controllers" }
            );
        }
    }
}
