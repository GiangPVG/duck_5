﻿using ShopBug.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopBug.Web.Models
{
    [Serializable]
    public class CartItem
    {
        public Product ProductSize { set; get; }
        public Product Product { get; set; }
        public Customer Customer { get; set; }
        public Order Order { get; set; }
        public int Quantity { get; set; }
        public OrderDetail OrderDetail { get; set; }
        public long OrderID { get; set; }
    }
}