﻿using ShopBug.Model.Dao;
using ShopBug.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopBug.Web.Areas.Admin.Controllers
{
    public class FooterController : Controller
    {
        // GET: Admin/Footer
        public ActionResult Index()
        {
            var dao = new FooterDao();
            var result = dao.ListAllFooter();
            return View(result);
        }

        [HttpGet]
        public ActionResult Edit(string id) {
            var footer = new FooterDao().ViewDetails(id);
            return View(footer);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(Footer footer)
        {
            var dao = new FooterDao();
            var result = dao.Update(footer);
            if (result)
            {
                return RedirectToAction("Index", "Footer");
            }
            else
            {
                ModelState.AddModelError("", "Update Footer not successfully");
            }

            return View("Index");
        }

    }
}