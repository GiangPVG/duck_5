﻿using ShopBug.Model.Dao;
using ShopBug.Model.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopBug.Web.Areas.Admin.Controllers
{
    public class SlideController : Controller
    {
        // GET: Admin/Slide
        public ActionResult Index()
        {
            var result = new SlideDao().ListAllAdmin();
            return View(result);
        }

        [HttpDelete]
        public ActionResult Delete(int id)
        {

            new SlideDao().Delete(id);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult ChangeStatus(int id)
        {
            var result = new SlideDao().ChangeStatus(id);
            return Json(new
            {
                status = result
            });
        }

        [HttpGet]
        public ActionResult Create()
        {
            Slide slide = new Slide();
            return View(slide);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(Slide slide, HttpPostedFileBase fileUpload)
        {

            var dao = new SlideDao();
            var filename = Path.GetFileName(fileUpload.FileName);
            var path = Path.Combine(Server.MapPath("/Assets/client/images/kamitos/"), filename);

            if (System.IO.File.Exists(path))
            {
                ViewBag.ThongBao = "Hình ảnh đã tồn tại";
            }
            else if (fileUpload == null)
            {
                ViewBag.ThongBao = "Chọn hình ảnh";
                return View();
            }
            else
            {
                fileUpload.SaveAs(path);
            }

            slide.Image = "/Assets/client/images/kamitos/" + fileUpload.FileName;

            long id = dao.Insert(slide);
            if (id > 0)
            {
                return RedirectToAction("Index", "Slide");
            }
            return View("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var slide = new SlideDao().ViewDetail(id);
            return View(slide);
        }

        [HttpPost]
        public ActionResult Edit(Slide slide, HttpPostedFileBase fileUpload)
        {
            var dao = new SlideDao();
            var filename = Path.GetFileName(fileUpload.FileName);
            var path = Path.Combine(Server.MapPath("/Assets/client/images/kamitos/"), filename);
            if (System.IO.File.Exists(path))
            {
                ViewBag.ThongBao = "Hình ảnh đã tồn tại";
            }
            else if (fileUpload == null)
            {
                ViewBag.ThongBao = "Chọn hình ảnh";
                return View();
            }
            else
            {
                fileUpload.SaveAs(path);
            }
            slide.Image = "/Assets/client/images/kamitos/" + fileUpload.FileName;

            var result = dao.Update(slide);
            if (result)
            {
                return RedirectToAction("Index", "Slide");
            }
            else
            {
                ModelState.AddModelError("", "Update Slide not successfully");
            }

            return View("Index");
        }
    }
}