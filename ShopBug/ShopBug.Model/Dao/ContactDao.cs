﻿using ShopBug.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopBug.Model.Dao
{
    public class ContactDao
    {
        ShopBugDbContext db = null;
        public ContactDao()
        {
            db = new ShopBugDbContext();

        }

        public Contact GetContact()
        {
            return db.Contacts.Single(x => x.Status == true);
        }

        public int InsertFeedBack(FeedBack fb)
        {
            db.FeedBacks.Add(fb);
            db.SaveChanges();
            return fb.ID;
        }

        public bool Delete(int id)
        {
            try
            {
                var feedback = db.FeedBacks.Find(id);
                db.FeedBacks.Remove(feedback);
                db.SaveChanges();
                return true;

            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
