﻿using ShopBug.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopBug.Model.Dao
{
    public class OrderDetailDao
    {
        ShopBugDbContext db = null;
        public OrderDetailDao()
        {
            db = new ShopBugDbContext();
        }
        public bool InsertOrderDetail(OrderDetail Details)
        {
            try
            {
                db.OrderDetails.Add(Details);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }

        }
        public OrderDetail ViewDetail(long idorder)
        {
            return db.OrderDetails.Find(idorder);
        }

    }
}

