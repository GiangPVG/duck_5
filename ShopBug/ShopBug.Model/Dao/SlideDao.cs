﻿using ShopBug.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopBug.Model.Dao

{
    public class SlideDao
    {
        ShopBugDbContext db = null;
        public SlideDao()
        {
            db = new ShopBugDbContext();
        }

        public List<Slide> ListAll()
        {
            return db.Slides.Where(x => x.Status == true).OrderBy(y => y.DispalyOrder).ToList();
        }


        public List<Slide> ListAllAdmin()
        {
            return db.Slides.OrderByDescending(x => x.CreatedDate).OrderBy(y => y.DispalyOrder).ToList();
        }

        public bool Delete(int id)
        {
            try
            {
                var slide = db.Slides.Find(id);
                db.Slides.Remove(slide);
                db.SaveChanges();
                return true;

            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool ChangeStatus(int id)
        {
            var slide = db.Slides.Find(id);
            slide.Status = !slide.Status;
            db.SaveChanges();

            return !slide.Status;
        }

        public long Insert(Slide entity)
        {
            db.Slides.Add(entity);
            db.SaveChanges();
            return entity.ID;
        }

        public Slide ViewDetail(int id)
        {
            return db.Slides.Find(id);
        }

        public bool Update(Slide entity)
        {
            try
            {
                var slide = db.Slides.Find(entity.ID);
                slide.Image = entity.Image;
                slide.DispalyOrder = entity.DispalyOrder;
                slide.URL = entity.URL;
                slide.CreatedDate = entity.CreatedDate;
                slide.ModifiedDate = DateTime.Now;
                slide.Status = entity.Status;
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
