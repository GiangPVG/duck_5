﻿using ShopBug.Model;
using ShopBug.Model.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;

namespace ShopBug.Web.Controllers
{
    public class ProductController : Controller
    {
        ShopBugDbContext db = new ShopBugDbContext();
        // GET: ProductController
        public ActionResult Index(int? page)
        {
            var productDao = new ProductDao();
            var pageNumber = page ?? 1;
            var pageSize = 6;

            ViewBag.SellProduct = productDao.ListSellProduct(6);
            var model = db.Products.OrderByDescending(x => x.ID).ToPagedList(pageNumber, pageSize);
            return View(model);
        }

        public ActionResult Test(int? page)
        {
            var productDao = new ProductDao();
            var pageNumber = page ?? 1;
            var pageSize = 6;

            ViewBag.SellProduct = productDao.ListSellProduct2(6);
            var model = db.Products.OrderByDescending(x => x.ID).ToPagedList(pageNumber, pageSize);
            return View(model);
        }

        public ActionResult Detail(long id)
        {
            var getSize = db.Sizes.ToList();
            SelectList list = new SelectList(getSize, "Id", "Name");
            ViewBag.SizeName = list;
            var product = new ProductDao().ViewDetail(id);
            return View(product);
        }

        public JsonResult ListName(string q)
        {
            var data = new ProductDao().ListName(q);
            return Json(new
            {
                data = data,
                status = true
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Search(string keyword, int page = 1, int pageSize = 1)
       {
            int totalRecord = 0;
            var model = new ProductDao().Search(keyword, ref totalRecord, page, pageSize);

            ViewBag.Total = totalRecord;
            ViewBag.Page = page;
            ViewBag.Keyword = keyword;
            int maxPage = 5;
            int totalPage = 0;

            totalPage = (int)Math.Ceiling((double)(totalRecord / pageSize));
            ViewBag.TotalPage = totalPage;
            ViewBag.MaxPage = maxPage;
            ViewBag.First = 1;
            ViewBag.Last = totalPage;
            ViewBag.Next = page + 1;
            ViewBag.Prev = page - 1;

            return View(model);
        }
    }
}