﻿using ShopBug.Model;
using ShopBug.Web.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopBug.Web.Areas.Admin.Controllers
{
    public class HomeController : BaseController
    {
        private const string Thongke = "Thongke";
        // GET: Admin/Home
        ShopBugDbContext db = new ShopBugDbContext();

        public ActionResult Index()
        {
            ViewBag.ThongkeDoanhThu = ThongKeDoanhThu();
            ViewBag.ThongKeDonhang = Donhang();
            ViewBag.ThongKeSanPham = ThongkeSP();
            ViewBag.ThongKeTheoNgay = ThongKeDonTheoNgay();
            //ViewBag.ThongKeThang = ThongKeDoanhThuTheoThang();
            return View();
        }

        public double Donhang()
        {
            double sldh = db.Orders.Count();
            return sldh;
        }

        public int ThongkeSP()
        {
            int sanpham = db.Products.Count();
            return sanpham;
        }

        public decimal ThongKeDoanhThu()
        {
            decimal TongDoanhThu = db.OrderDetails.Sum(n => n.Quantity * n.Price).Value;
            return TongDoanhThu;
        }


        public decimal ThongKeDonTheoNgay()
        {
            var list = db.Orders.Where(n => n.ModifiedDate == DateTime.Now);
            decimal TongTien = 0;
            foreach (var item in list)
            {
                TongTien += decimal.Parse(item.OrderDetails.Sum(n => n.Quantity * n.Price).Value.ToString());
            }

            return TongTien;
        }

        [HttpPost]
        public ActionResult ThongKeDoanhThuTheoThang(int Thang, int Nam)
        {
            ViewBag.ThongkeDoanhThu = ThongKeDoanhThu();
            ViewBag.ThongKeTheoNgay = ThongKeDonTheoNgay();
            ViewBag.ThongKeDonhang = Donhang();
            ViewBag.ThongKeSanPham = ThongkeSP();
            var lstDDH = db.Orders.Where(n => n.ModifiedDate.Value.Month == Thang && n.ModifiedDate.Value.Year == Nam);
            decimal TongTien = 0;
            foreach (var item in lstDDH)
            {
                TongTien += decimal.Parse(item.OrderDetails.Sum(n => n.Quantity * n.Price).Value.ToString());
            }
            var session = new Thongke();
            session.Price = TongTien;
            var list = new List<Thongke>();
            Session[Thongke] = list;
            ViewBag.Thongke = TongTien;
            return View();
        }
    }
}