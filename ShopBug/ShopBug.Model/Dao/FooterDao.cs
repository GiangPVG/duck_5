﻿using ShopBug.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopBug.Model.Dao
{
    public class FooterDao
    {
        ShopBugDbContext db = null;
        public FooterDao()
        {
            db = new ShopBugDbContext();
        }

        public Footer GetFooter()
        {
            return db.Footers.SingleOrDefault(x => x.Status == true);
        }

        public List<Footer> ListAllFooter() {
            return db.Footers.Where(x => x.Status == true).ToList();
        }

        public Footer ViewDetails(string id) {
            return db.Footers.Find(id);
        }

        public bool Update(Footer entity) {
            try
            {

                var footer = db.Footers.Find(entity.ID);
                footer.Content = entity.Content;
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
    }
}
