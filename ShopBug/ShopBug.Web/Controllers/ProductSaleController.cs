﻿using PagedList;
using ShopBug.Model;
using ShopBug.Model.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopBug.Web.Controllers
{
    public class ProductSaleController : Controller
    {
        ShopBugDbContext db = new ShopBugDbContext();
        // GET: ProductSale
        public ActionResult Index(int? page)
        {
            var productDao = new ProductDao();
            var pageNumber = page ?? 1;
            var pageSize = 6;

            ViewBag.Sale = productDao.ListSale();
            ViewBag.SellProduct = productDao.ListSellProduct(4);
            var model = db.Products.OrderByDescending(x => x.ID).ToPagedList(pageNumber, pageSize);
            return View(model);
        }
    }
}