﻿using ShopBug.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using ShopBug.Model.ViewModel;

namespace ShopBug.Model.Dao
{
    public class ProductDao
    {
        ShopBugDbContext db = null;

        public ProductDao()
        {
            db = new ShopBugDbContext();
        }
        public Product ViewDetail(long id)
        {
            return db.Products.Find(id);
        }
        public List<Product> ListSellProduct(int top)
        {
            //return db.Products.OrderByDescending(x => x.CreatedDate).Take(top).ToList();
            return db.Products.Where(x => x.Status == true & x.PromotionPrice == x.Price).OrderBy(p => Guid.NewGuid()).Take(top).ToList();
        }

        public List<Product> ListSellProduct2(int top)
        {
            //return db.Products.OrderByDescending(x => x.CreatedDate).Take(top).ToList();
            return db.Products.Where(x => x.Status == false & x.PromotionPrice == x.Price).OrderBy(p => Guid.NewGuid()).Take(top).ToList();
        }

        public List<Product> ListSale() {
            return db.Products.Where(x => x.PromotionPrice > x.Price).ToList();
        }

        public List<string> ListName(string keyword)
        {
            return db.Products.Where(x => x.Name.Contains(keyword)).Select(x => x.Name).ToList();
        }

        //public List<Product> ListPage(int? page) {
        //    var pageNumber = page ?? 1;
        //    var pageSize = 8;
        //    return db.Products.OrderByDescending(x => x.ID).ToPagedList(pageNumber, pageSize).ToList();

        //}

        public List<Product> ListRandom(int dandom) {
            return db.Products.Where(x => x.CreatedDate < DateTime.Now).OrderBy(p => Guid.NewGuid()).Take(dandom).ToList();
        }


        public IEnumerable<Product> Details()
        {
            return db.Products.OrderByDescending(x => x.ID).ToList();
        }

        // Admin

        public IEnumerable<Product> ListAllPaging(string searchString, int page, int pageSize)
        {
            IQueryable<Product> model = db.Products;
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.Name.Contains(searchString) || x.Name.Contains(searchString));
            }

            return model.OrderByDescending(x => x.CreatedDate).ToPagedList(page, pageSize);
        }

        public long Insert(Product entity) {
            db.Products.Add(entity);
            db.SaveChanges();
            return entity.ID;
        }

        public bool Delete(int id)
        {

            try
            {
                var productdetail = db.OrderDetails.Where(x => x.ProductID.Equals(id)).ToList();
                foreach (var item in productdetail)
                {
                    db.OrderDetails.Remove(item);
                    db.SaveChanges();
                }
                var product = db.Products.Find(id);
                db.Products.Remove(product);
                db.SaveChanges();
                return true;

            }
            catch (Exception e)
            {
                return false;
            }


        }

        public bool ChangeStatus(long id)
        {
            var product = db.Products.Find(id);
            product.Status = !product.Status;
            db.SaveChanges();

            return !product.Status;
        }

        public bool Update(Product entity) {
            try
            {
                var products = db.Products.Find(entity.ID);
                products.Name = entity.Name;
                products.MetaTitle = entity.MetaTitle;
                products.Image = entity.Image;
                products.Price = entity.Price;
                products.Warranty = entity.Warranty;
                products.Description = entity.Description;
                products.ModifiedDate = DateTime.Now;
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<ProductViewModel> Search(string keyword, ref int totalRecord, int pageIndex = 1, int pageSize = 2)
        {
            totalRecord = db.Products.Where(x => x.Name == keyword).Count();
            var model = (from a in db.Products
                         where a.Name.Contains(keyword)
                         select new
                         {
                             CreatedDates = a.CreatedDate,
                             Id = a.ID,
                             Images = a.Image,
                             Names = a.Name,
                             MetaTitles = a.MetaTitle,
                             Prices = a.Price
                         }).AsEnumerable().Select(x => new ProductViewModel()
                         {
                             CreatedDate = x.CreatedDates,
                             ID = x.Id,
                             Images = x.Images,
                             Name = x.Names,
                             MetaTitle = x.MetaTitles,
                             Price = x.Prices
                         });
            model.OrderByDescending(x => x.CreatedDate).Skip((pageIndex - 1) * pageSize).Take(pageSize);
            return model.ToList();
        }
    }
}
