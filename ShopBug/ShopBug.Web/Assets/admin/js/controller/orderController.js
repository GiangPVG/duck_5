﻿var user = {
    init: function () {
        user.registerEvents();
    },
    registerEvents: function () {
        $('.btn-active').off('click').on('click', function (e) {
            e.preventDefault();
            var btn = $(this);
            var id = btn.data('id');
            $.ajax({
                url: "/Admin/Order/ChangeStatus",
                data: { id: id },
                datatype: "json",
                type: "POST",

                success: function (response) {
                    console.log(response);
                    if (response.status === true) {
                        btn.text('Đã xác nhận');
                    } else {
                        btn.text('Xác nhận thanh toán');
                    }
                }
            });
        });
    }
};
user.init();


var product = {
    init: function () {
        product.registerEvents();
    },
    registerEvents: function () {
        $('.btn-status').off('click').on('click', function (e) {
            e.preventDefault();
            var btn = $(this);
            var id = btn.data('id');
            $.ajax({
                url: "/Admin/Product/ChangeStatus",
                data: { id: id },
                datatype: "json",
                type: "POST",

                success: function (response) {
                    console.log(response);
                    if (response.status === true) {
                        btn.text('Đã xác nhận');
                    } else {
                        btn.text('Chưa xác nhận');
                    }
                }
            });
        });
       
    }
};
product.init();

var customer = {
    init: function () {
        customer.registerEvents();
    },
    registerEvents: function () {
        $('.btn-status2').off('click').on('click', function (e) {
            e.preventDefault();
            var btn = $(this);
            var id = btn.data('id');
            $.ajax({
                url: "/Admin/Customer/ChangeStatus",
                data: { id: id },
                datatype: "json",
                type: "POST",

                success: function (response) {
                    console.log(response);
                    if (response.status === true) {
                        btn.text('Đã xác nhận');
                    } else {
                        btn.text('Chưa xác nhận');
                    }
                }
            });
        });
    }
};
customer.init();

var menu = {
    init: function () {
        menu.registerEvents();
    },
    registerEvents: function () {
        $('.btn-menu').off('click').on('click', function (e) {
            e.preventDefault();
            var btn = $(this);
            var id = btn.data('id');
            $.ajax({
                url: "/Admin/Menu/ChangeStatus",
                data: { id: id },
                datatype: "json",
                type: "POST",

                success: function (response) {
                    console.log(response);
                    if (response.status === true) {
                        btn.text('Đã xác nhận');
                    } else {
                        btn.text('Chưa xác nhận');
                    }
                }
            });
        });
    }
};
menu.init();

var slide = {
    init: function () {
        slide.registerEvents();
    },
    registerEvents: function () {
        $('.btn-sta').off('click').on('click', function (e) {
            e.preventDefault();
            var btn = $(this);
            var id = btn.data('id');
            $.ajax({
                url: "/Admin/Slide/ChangeStatus",
                data: { id: id },
                datatype: "json",
                type: "POST",

                success: function (response) {
                    console.log(response);
                    if (response.status === true) {
                        btn.text('Đã xác nhận');
                    } else {
                        btn.text('Chưa xác nhận');
                    }
                }
            });
        });
    }
};
slide.init();