﻿using ShopBug.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;
using System.Web.Mvc;
using ShopBug.Model.Dao;

namespace ShopBug.Web.Areas.Admin.Controllers
{
    public class OrderController : Controller
    {
        ShopBugDbContext db = new ShopBugDbContext();
        // GET: Admin/Order
        public ActionResult Index(int? page)
        {
            var pageNumber = page ?? 1;
            var pageSize = 8;
            var orderList = db.Orders.OrderByDescending(x => x.ID).ToPagedList(pageNumber, pageSize);

            return View(orderList);
        }

        public ActionResult Details(int? id) {

            var order = db.Orders.Find(id);
            if (order == null) {
                return HttpNotFound();
            }

            return View(order);
        }

        [HttpDelete]
        public ActionResult Delete(int id)
        {

            new OrderDao().Delete(id);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult ChangeStatus(long id)
        {
            var result = new OrderDao().ChangeStatus(id);
            return Json(new
            {
                status = result
            });
        }


    }
}