﻿using ShopBug.Model.Dao;
using ShopBug.Model.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace ShopBug.Web.Controllers
{
    public class ContactController : Controller
    {
        // GET: Contact
        public ActionResult Index()
        {
            var model = new ContactDao().GetContact();
            return View(model);
        }

        [HttpPost]
        public JsonResult Send(string name, string mobile, string email, string address, string content)
        {
            var feedback = new FeedBack();
            feedback.Name = name;
            feedback.Email = email;
            feedback.CreatedDate = DateTime.Now;
            feedback.Phone = mobile;
            feedback.Address = address;
            feedback.Content = content;
            var id = new ContactDao().InsertFeedBack(feedback);
            if (id > 0)
            {
                return Json(new
                {
                    status = true

                });
                //Send mail

            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }
        }

        [HttpPost]
        public ActionResult SendMail(string name, string phone, string email, string address, string content)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var emailname = name;
                    var phonename = phone;
                    var sendname = new MailAddress(email, "Email");
                    var addressname = address;
                    var contentname = content;

                    var senderemail = new MailAddress("nguyenhung291095@gmail.com", name);
                    var password = "Hhhh1234";


                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(senderemail.Address, password)

                    };

                    using (var mess = new MailMessage(sendname, senderemail)
                    {
                        Subject = name + ": Feedback Khách hàng",
                        Body = "Email Feedback :" + email + " " + " Nội Dung :" + content
                    }
                    )
                    {

                            smtp.Send(mess);
                    }

                    using (var mess2 = new MailMessage(senderemail, sendname)
                    {
                        Subject = name + ": Feedback Khách hàng",
                        Body = "Email Feedback :" + email + " " + " Nội Dung :" + content+ 
                        " Cảm ơn Feedback của bạn"
                    }
                    )
                    {

                        smtp.Send(mess2);
                    }



                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex + "Không thể gửi mail";
            }
            return Redirect("/lien-he");
        }
    }
}