﻿using ShopBug.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using ShopBug.Model.Dao;
using ShopBug.Model.Models;

namespace ShopBug.Web.Areas.Admin.Controllers
{
    public class CustomerController : Controller
    {
        ShopBugDbContext db = new ShopBugDbContext();

        // GET: Admin/Customer
        public ActionResult Index(string searchString, int page = 1, int pageSize = 8)
        {
            var dao = new CustomerDao();
            var model = dao.ListAllPaging(searchString, page, pageSize);

            ViewBag.SearchString = searchString;
            return View(model);
        }

        [HttpPost]
        public JsonResult ChangeStatus(long id)
        {
            var result = new CustomerDao().ChangeStatus(id);
            return Json(new
            {
                status = result
            });
        }

        [HttpDelete]
        public ActionResult Delete(int id)
        {

            new CustomerDao().Delete(id);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(long id) {
            var customer = new CustomerDao().ViewDetail(id);
            return View(customer);
        }

        [HttpPost]
        public ActionResult Edit(Customer customer) {
            var dao = new CustomerDao();
            var result = dao.Update(customer);
            if (result) {
                return RedirectToAction("Index", "Customer");
            }
            else
            {
                ModelState.AddModelError("", "Update Customer not successfully");
            }

            return View("Index");
        }
    }
}