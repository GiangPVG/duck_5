﻿using ShopBug.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopBug.Model.Dao
{
    public class FeedbackDao
    {
        ShopBugDbContext db = null;
        public FeedbackDao() {
            db = new ShopBugDbContext();
        }


        public List<FeedBack> ListAll() {
            return db.FeedBacks.OrderByDescending(x => x.CreatedDate).ToList();
        }
    }
}
